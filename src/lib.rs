use std::collections::HashMap;
use peg;


#[derive(Debug, Default)]
pub struct Start {
  pub header : Header,
  pub sfm_list : Vec<SFM>,
}


// Header

#[derive(Debug, Default)]
pub struct Header {
  pub modules_name_list : Vec<String>,
  pub sampling_rate_list : Vec<f64>,
}

#[derive(Debug)]
pub enum HeaderLine {
  SpaceOrHashLine,
  DoNotEditLine,
  ModulesNoNameListLine,
  ModulesNameListLine(Vec<String>),
  SamplingRateLine(f64),
}


// SFM

#[derive(Debug, Default)]
pub struct SFM {
  pub sfm_name : String,
  pub sampling_rate_line : Option<SamplingRateLine>,
  pub design_list : Vec<Design>,
  pub filter_list : Vec<Filter>,
}

#[derive(Debug, Default)]
pub struct SamplingRateLine {
  pub sfm_name : String,
  pub sampling_rate : f64,
}

#[derive(Debug, Default)]
pub struct Design {
  pub sfm_name : String,
  pub filter_number : i64,
  pub design_string : String,
}

#[derive(Debug, Default)]
pub struct Filter {
  pub sfm_name : String,
  pub filter_number : i64,
  pub filter_switching : i64,
  pub number_of_sos : i64,
  pub ramp : f64,
  pub timeout : f64,
  pub filter_name : String,
  pub filter_gain : f64,
  pub sos_list : Vec<[f64; 4]>,
}

#[derive(Debug)]
pub enum SFMLine {
  SFM(SFM),
  SpaceOrHashLine,
}


#[cxx::bridge]
pub mod ffi {

  extern "Rust" {
    fn parse(text : &str) -> Result<Start, peg::error::ParseError<peg::str::LineCol>>;

    fn generate_file(start : Start) -> String;
  }
}



peg::parser!{
  grammar parser() for str {
    rule space() -> () = [' '] {}
    rule eol() -> () = ['\n'] {}

    rule hash() -> () = ['#'] {}
    rule back_slash() -> () = ['\\'] {}

    rule char() -> char = ['a'..='z' | 'A'..='Z']
    rule digit() -> char = ['0'..='9']
    rule underscore() -> char = ['_']

    rule sign() -> char = ['-'] / ['+']
    rule period() -> char = ['.']
    rule exp() -> char = ['e'] / ['E']

    rule name() -> String = cs:$( char() ( char() / digit() / underscore() )* ) { String::from(cs) }
    rule integer() -> i64 = cs:$( sign()? digit()+ ) {? cs.parse::<i64>().or(Err("i64")) }
    rule number() -> f64 = cs:$( sign()? ( digit()+ ( period() digit()* )? / period() digit()+ ) ( exp() sign()? digit()+ )? ) {? cs.parse::<f64>().or(Err("f64")) }

    rule not_space_or_eol() -> char = !( space() / eol() ) c:[_] { c }
    rule one_or_more_not_space_or_eol() -> String = cs:not_space_or_eol()+ { cs.into_iter().collect() }

    rule not_back_slash_or_eol() -> char = !( back_slash() / eol() ) c:[_] { c }
    rule one_or_more_not_back_slash_or_eol() -> String = cs:not_back_slash_or_eol()+ { cs.into_iter().collect() }

    rule space_or_hash() -> () = ( space() / hash() ) {}
    rule space_or_hash_line() -> () = space_or_hash()* eol() {}


    // SFM

    rule sos() -> [f64; 4] = x1:number() space()+ x2:number() space()+ x3:number() space()+ x4:number() { [x1, x2, x3, x4] }
    rule sos_line() -> [f64; 4] = space()* x:sos() space()* eol() { x }
    rule sos_list() -> Vec<[f64; 4]> = x:sos() space()* eol() xs:sos_line()* { let mut a = xs.clone(); a.insert(0, x); return a; }

    rule filter() -> Filter =
      sfm_name:name() space()+
      filter_number:integer() space()+
      filter_switching:integer() space()+
      number_of_sos:integer() space()+
      ramp:number() space()+
      timeout:number() space()+
      filter_name:one_or_more_not_space_or_eol() space()+
      filter_gain:number() space()+
      sos_list:sos_list()
      {
        Filter { sfm_name, filter_number, filter_switching, number_of_sos, ramp, timeout, filter_name, filter_gain, sos_list }
      }

      rule design_string_break() -> () = back_slash() space()* eol() hash() {}
      rule design_string_part() -> String = one_or_more_not_back_slash_or_eol()
      rule design_string() -> String = xs:( design_string_part() ++ design_string_break() ) { xs.join("") }
      rule design() -> Design = "# DESIGN" space()+ sfm_name:name() space()+ filter_number:integer() space()+ design_string:design_string() space()* eol()
      {
        Design { sfm_name, filter_number, design_string }
      }

      rule sfm_sampling_rate_line() -> SamplingRateLine = space_or_hash()* "SAMPLING" space()+ sfm_name:name() space()+ sampling_rate:number() space_or_hash()* eol()
      {
        SamplingRateLine { sfm_name, sampling_rate }
      }

      rule sfm_name_line() -> String = space_or_hash()* sfm_name:name() space_or_hash()* eol() { sfm_name }

      rule sfm() -> SFM = sfm_name:sfm_name_line() space_or_hash_line()* sampling_rate_line:sfm_sampling_rate_line()? space_or_hash_line()* design_list:design()* space_or_hash_line()* filter_list:filter()*
      {
        SFM { sfm_name, sampling_rate_line, design_list, filter_list }
      }


      rule sfm_sfm_line() -> SFMLine = x:sfm() { SFMLine::SFM(x) }

      rule space_or_hash_sfm_line() -> SFMLine = space_or_hash_line() { SFMLine::SpaceOrHashLine }

      rule sfm_list() -> Vec<SFM> = xs:(sfm_sfm_line() / space_or_hash_sfm_line())*
      {
        let mut sfm_list : Vec<SFM> =  vec![];

        for x in xs {
          match x {
            SFMLine::SFM(sfm) => { sfm_list.push(sfm) }
            SFMLine::SpaceOrHashLine => { continue }
          }
        }

        return sfm_list;
      }


      // Header

      rule space_or_hash_header_line() -> HeaderLine = space_or_hash_line() { HeaderLine::SpaceOrHashLine }

      rule do_not_edit_header_line() -> HeaderLine = "# Computer generated file: DO NOT EDIT" space()* eol() { HeaderLine::DoNotEditLine }

      rule modules_no_name_list_header_line() -> HeaderLine = "# MODULES" space()* eol() { HeaderLine::ModulesNoNameListLine }

      rule space_delimited_name_list() -> Vec<String> = xs:(name() ** space()) { xs }
      rule modules_name_list_header_line() -> HeaderLine = "# MODULES" space()+ sfm_name_list:space_delimited_name_list() space()* eol() { HeaderLine::ModulesNameListLine(sfm_name_list) }

      rule sampling_rate_header_line() -> HeaderLine = "# SAMPLING RATE" space()+ sampling_rate:number() space()* eol() { HeaderLine::SamplingRateLine(sampling_rate) }

      rule header_line() -> HeaderLine = space_or_hash_header_line() / do_not_edit_header_line() / modules_no_name_list_header_line() / modules_name_list_header_line() / sampling_rate_header_line()

      rule header() -> Header = xs:header_line()*
      {
        let mut modules_name_list_list : Vec<Vec<String>> = vec![];
        let mut sampling_rate_list : Vec<f64> = vec![];

        for x in xs {
          match x {
            HeaderLine::SpaceOrHashLine => { continue }
            HeaderLine::DoNotEditLine => { continue }
            HeaderLine::ModulesNoNameListLine => { continue }
            HeaderLine::ModulesNameListLine(modules_name_list) => { modules_name_list_list.push(modules_name_list); }
            HeaderLine::SamplingRateLine(sampling_rate) => { sampling_rate_list.push(sampling_rate); }

          }
        }

        let modules_name_list = modules_name_list_list.into_iter().flatten().collect();

        return Header { modules_name_list, sampling_rate_list };
      }


      // Start


      rule magic_line() -> () = "# FILTERS FOR ONLINE SYSTEM" space()* eol()

      pub rule start() -> Start = magic_line() header:header() sfm_list:sfm_list()
      {
        Start { header, sfm_list }
      }
  }
}



pub fn parse(text :&str) -> () {
  pub struct Filter {
    pub number : i64,
    pub switching : i64,
    pub number_of_sos : i64,
    pub ramp : f64,
    pub timeout : f64,
    pub name : String,
    pub gain : f64,
    pub sos_list : Vec<[f64; 4]>,

    pub design_string : String,
  }

  pub struct SFM {
    pub sfm_name : String,
    pub filter_list : Vec<Filter>,
  }

  pub struct File {
    pub modules_name_list : Vec<String>,
    pub sampling_rate : f64,
    pub module_list : Vec<SFM>,
  }

  let result =  parser::start(text);

  if let Ok(start) = result {
    let module_name_list = start.header.modules_name_list;
    let sampling_rate = start.header.sampling_rate_list.first().expect("missing sampling rate");
    for sfm in start.sfm_list {
      let name = sfm.sfm_name;

      let mut design_map: HashMap<i64, Design> = HashMap::new();
      for design in sfm.design_list {
        design_map.insert(design.filter_number, design);
      }

      let mut filter_map: HashMap<i64, Filter> = HashMap::new();
      for filter in sfm.filter_list {
        if let Some(design) = design_map.get(&filter.filter_number) {
          let f = Filter {
            number : filter.filter_number,
            switching : filter.filter_switching,
            number_of_sos : filter.number_of_sos,
            ramp : filter.ramp,
            timeout : filter.timeout,
            name : filter.filter_name,
            gain : filter.filter_gain,
            sos_list : filter.sos_list,
            design_string : design.design_string
          };
        }
        
      }


    }
  }
}


pub fn generate_file(start : Start) -> String {
  let mut str = String::new();

  str.push_str("# FILTERS FOR ONLINE SYSTEM\n#\n");
  str.push_str("# Computer generated file: DO NOT EDIT\n#\n");

  if !start.header.modules_name_list.is_empty() {
    str.push_str(&std::format!("# MODULES {}\n#\n", start.header.modules_name_list.join(" ")));
  }

  for sampling_rate in start.header.sampling_rate_list {
    str.push_str(&std::format!("# SAMPLING RATE {}\n", sampling_rate));
  }

  str.push_str("#\n");
  for sfm in start.sfm_list {
    str.push_str(&std::format!("{}\n", "#".repeat(80)));

    str.push_str(&std::format!("### {:width$} ###\n", sfm.sfm_name, width = 72));

    str.push_str(&std::format!("{}\n", "#".repeat(80)));

    if let Some(sampling_rate_line) = sfm.sampling_rate_line {
      str.push_str(&std::format!("# SAMPLING {} {}\n", sampling_rate_line.sfm_name, sampling_rate_line.sampling_rate));
    }

    for design in sfm.design_list {
      str.push_str(&std::format!("# DESIGN {} {} {}\n", design.sfm_name, design.filter_number, design.design_string));
    }

    str.push_str(&std::format!("### {} ###\n", " ".repeat(72)));

    for filter in sfm.filter_list {
      str.push_str(&std::format!("{:<8} {} {:<2} {} {:>6} {:>6} {:<10} {:26.24e} ", filter.sfm_name, filter.filter_number, filter.filter_switching, filter.number_of_sos, filter.ramp, filter.timeout, filter.filter_name, filter.filter_gain));

      for sos in filter.sos_list {
        str.push_str(&std::format!("{:20.16} {:20.16} {:20.16} {:20.16}\n", sos[0], sos[1], sos[2], sos[3]));
      }
    }
    str.push('\n');
  }

  return str;
}
