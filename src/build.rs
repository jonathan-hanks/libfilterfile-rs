
fn main() {
  cxx_build::bridge("src/lib.rs")  // returns a cc::Build
    .file("src/parse.cc")
    .flag_if_supported("-std=c++11")
    .compile("cxxbridge-parse");

  println!("cargo:rerun-if-changed=src/lib.rs");
}
